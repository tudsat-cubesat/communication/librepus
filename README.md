# ECSS Package Utilization Standard (PUS)

This repository contains a implementation of the ECSS Package Utilization Standard (PUS) in C. 

A collection of usefull tools for integration testing with the [OSDLP](https://gitlab.com/librespacefoundation/osdlp) and general devlopments can be found in [LibrePUS-dev](https://gitlab.com/tudsat-cubesat/communication/librepus-dev).

## Features

Implemented:
- Service 1 Request verification
- Service 5: Event reporting
- Service 8 Function management

Planned:
- Service 3 Housekeeping

## Build

The only requirement to build the library are `gcc` and `make`. 
Additional requirement to build tests are `libgtest-dev` ad `gcovr`.

Clone the repository and use `make all` to build the library.

## Contributing

Make sure to run `astyle` on all updated files before commiting, otherwise the CI pipeline will fail.

Only merge requests that pass the pipeline will be accepted.

## License

© 2021 [TU Darmstadt Space Technology e.V.](http://www.tudsat.space) and contributors

Licensed under the [GPLv3](./LICENSE).

