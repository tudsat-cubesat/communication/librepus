/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Service type ST[05] event reporting
 */

#ifndef INC_ST05_EV_REP_H_
#define INC_ST05_EV_REP_H_

#include <errno.h>
#include <stddef.h>
#include <stdint.h>

#include "lpus_common.h"

/* TODO: Add configurable size of event ID */
/** Type of event definition ID */
typedef uint8_t event_id_t;

/** ST[05] subservices */
typedef enum st05_subservice {
    TM0501_EV_INFO = 0x0501, /**< TM[5,1] informative event report */
    TM0502_EV_LOW = 0x0502, /**< TM[5,2] low severity anomaly report */
    TM0503_EV_MEDIUM = 0x0503, /**< TM[5,3] medium severity anomaly report*/
    TM0504_EV_HIGH = 0x0504, /**< TM[5,4] high severity anomaly report */
    TC0505_EN_REP = 0x0505, /**< TC[5,5] enable report generation */
    TC0506_DIS_REP = 0x0506, /**< TC[5,6] disable report generation */
    TC0507_REQ_DIS_LIST = 0x0507, /**< TC[5,7] request list of disabled events */
    TM0508_RESP_DIS_LIST = 0x0508, /**< TM[5,8] respond with list of disabled events */
} st05_subservice_t;

/** Structure of event report TM[5,1] - TM[5,4] */
typedef struct tm05xx_ev_rep {
    event_id_t ev_id; /**< event definition ID */
    void* data; /**< auxiliary data */
    size_t data_size; /**< size of auxiliary data */
} tm05xx_ev_rep_t;

/** Structure of event lists TC[5,5], TC[5,6], TM[5,8] */
typedef struct tc05xx_ev_list {
    lpus_tc_ack_t tc_ack; /**< common ACK flags, oly for TC packets */
    /* TODO: Add configurable size of event list number */
    uint8_t num; /**< number of event IDs in the list */
    event_id_t* ev_list; /**< event IDs list */
} tc05xx_ev_list_t;

/** Structure of common TC ACK flags TC[5,7] */
typedef struct tc0507_flags {
    lpus_tc_ack_t tc_ack; /**< common TC ACK flags */
} tc0507_flags_t;

/** Union of all supported ST[05] subservice data structures */
typedef union st05_struct {
    tm05xx_ev_rep_t tm0501; /**< data structure for TM0501_EV_INFO */
    tm05xx_ev_rep_t tm0502; /**< data structure for TM0502_EV_LOW */
    tm05xx_ev_rep_t tm0503; /**< data structure for TM0503_EV_MEDIUM */
    tm05xx_ev_rep_t tm0504; /**< data structure for TM0504_EV_HIGH */
    tc05xx_ev_list_t tc0505; /**< data structure for TC0505_EN_REP */
    tc05xx_ev_list_t tc0506; /**< data structure for TC0506_DIS_REP */
    tc0507_flags_t tc0507; /**< data structure for TC0507_REQ_DIS_LIST */
    tc05xx_ev_list_t tm0508; /**< data structure for TM0508_RESP_DIS_LIST */
} st05_struct_t;

#endif /* INC_ST05_EV_REP_H_ */
