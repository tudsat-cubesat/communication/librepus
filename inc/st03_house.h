/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2021 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Service type ST[03] housekeeping
 */

#ifndef INC_ST03_HOUSE_H_
#define INC_ST03_HOUSE_H_

#include <errno.h>
#include <stddef.h>
#include <stdint.h>

#include "lpus_common.h"

#define ST03_REP_PARAMS_NUM_MAX 64  /**< Max number of parameters per report */

/** ST[03] subservices */
typedef enum st03_subservice {
    TM0325_HOUSE_PARAM = 0x0325, /**< TM[3,25] housekeeping parameter report */
} st03_subservice_t;

/** Single parameter inside parameter report's array */
typedef struct tm03xx_param {
    uint16_t id; /** parameter id */
    uint8_t size; /** size of parameter value */
    uint64_t val; /** parameter value */
} tm03xx_param_t;

/* TODO: Add configurable size of parameters */
/** Structure of parameter reports TM[3,xx] */
typedef struct tm03xx_param_rep {
    uint16_t struct_id; /**< parameter report structure ID */
    /* value of parameter report structure */
    uint8_t params_num; /** number of parameters in params array */
    tm03xx_param_t
    params[ST03_REP_PARAMS_NUM_MAX]; /** array of parameters for given struct_id */
} tm03xx_param_rep_t;

/** Union of all supported ST[03] subservice data structures */
typedef union st03_struct {
    tm03xx_param_rep_t tm0325; /**< data structure for TM0325_HOUSE_PARAM */
} st03_struct_t;

#endif /* INC_ST03_HOUSE_H_ */
