/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INC_LPUS_OSDLP_H_
#define INC_LPUS_OSDLP_H_

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Wrappers for LibrePUS over OSDLP library
 */

#include "osdlp.h"
#include "lpus.h"

/** PUS-independent Space Packet parameters */
typedef struct lpus_spacket_params {
    uint16_t apid; /**< application process ID (APID) */
    uint16_t seq_flag; /**< sequence flag */
    uint16_t seq_count; /**< sequence count or packet name */
} lpus_spacket_params_t;

/**
 * Encode Space Packet carrying PUS message
 *
 * @param[in] in_spacket_p Space Packet parameters
 * @param[in] in_sub PUS subservice enum
 * @param[in] in_struct PUS input data structure
 * @param[out] out_buf output buffer to store Space Packet
 * @param[in] buf_size size of output buffer
 *
 * @returns size of data in output buffer, or negative error code
 * @retval >= 0 size of data in output buffer
 * @retval -EINVAL invalid argument
 * @retval -EFBIG data too large to fit into output buffer
 * @retval -EFAULT failed to encode Space Packet primary header
 */
static inline int32_t lpus_spacket_encode(lpus_spacket_params_t* in_spacket_p,
        uint16_t in_sub, lpus_struct_t* in_struct,
        uint8_t* out_buf, size_t buf_size)
{
    struct spp_prim_hdr prim_hdr;
    int32_t pus_size;
    int32_t prim_hdr_size;

    if (!in_spacket_p || (buf_size < sizeof(struct spp_prim_hdr)))
        return (-EINVAL);

    /* PUS payload */
    pus_size = lpus_encode(in_sub, in_struct,
                           (out_buf + sizeof(struct spp_prim_hdr)),
                           (buf_size - sizeof(struct spp_prim_hdr)));
    if (pus_size < 0)
        return pus_size;

    /* Space Packet primary header */
    prim_hdr.version = LPUS_PACKET_VERSION;
    prim_hdr.is_tc = 0;
    prim_hdr.has_sec_hdr = 1;
    prim_hdr.apid = in_spacket_p->apid;
    prim_hdr.seq_flag = in_spacket_p->seq_flag;
    prim_hdr.seq_count = in_spacket_p->seq_count;
    prim_hdr.packet_data_len = (pus_size - 1);

    prim_hdr_size =  osdlp_spp_pack(&prim_hdr, NULL, out_buf,
                                    sizeof(struct spp_prim_hdr));
    if (prim_hdr_size < 0)
        return (-EFAULT);

    return (prim_hdr_size + pus_size);
}

/**
 * Decode Space Packet carrying PUS message

 * @param[in] in_buf input buffer that stores encoded Space Packet
 * @param[in] packet_size size of Space Packet in input buffer
 * @param[out] out_spacket_p Space Packet parameters
 * @param[out] sub decoded PUS subservice enum
 * @param[out] out_struct PUS decoded data structure
 *
 * @returns size of data parsed from input buffer, or negative error code
 * @retval >= 0 size of parsed data from input buffer
 * @retval -EINVAL invalid argument
 * @retval -EFAULT failed to decode Space Packet primary header
 */
static inline int32_t lpus_spacket_decode(uint8_t* in_buf, size_t packet_size,
        lpus_spacket_params_t* out_spacket_p,
        uint16_t* out_sub, lpus_struct_t* out_struct)
{
    struct spp_prim_hdr prim_hdr;
    int32_t pus_size;
    int32_t prim_hdr_size;

    if (!out_spacket_p || (packet_size < sizeof(struct spp_prim_hdr)))
        return (-EINVAL);

    /* Space Packet primary header */
    prim_hdr_size = osdlp_spp_unpack(&prim_hdr, in_buf, packet_size,
                                     NULL);
    if ((prim_hdr_size < 0) || (prim_hdr.version != LPUS_PACKET_VERSION) ||
        (prim_hdr.has_sec_hdr != 1))
        return (-EFAULT);

    /* parse primary header */
    pus_size = prim_hdr.packet_data_len + 1;
    if ((int32_t)packet_size < (prim_hdr_size + pus_size))
        return (-EINVAL);

    out_spacket_p->apid = prim_hdr.apid;
    out_spacket_p->seq_flag = prim_hdr.seq_flag;
    out_spacket_p->seq_count = prim_hdr.seq_count;

    /* PUS payload */
    pus_size = lpus_decode((in_buf + sizeof(struct spp_prim_hdr)), pus_size,
                           out_sub, out_struct);
    if (pus_size < 0)
        return pus_size;

    return (prim_hdr_size + pus_size);
}

#ifdef __cplusplus
}
#endif

#endif /* INC_LPUS_OSDLP_H_ */
