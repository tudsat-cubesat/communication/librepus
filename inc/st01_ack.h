/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Service type ST[01] request verification reports (ACK / NACK)
 */

#ifndef INC_ST01_ACK_H_
#define INC_ST01_ACK_H_

#include <errno.h>
#include <stddef.h>
#include <stdint.h>

#include "lpus_common.h"

/* TODO: Add configurable size of failure code */
/** Type of failure code */
typedef uint16_t fail_code_t;

/* TODO: Add configurable size of step ID */
/** Type of failure code */
typedef uint8_t step_id_t;

/** ST[01] subservices */
typedef enum st01_subservice {
    TM0101_SUCCESS_ACCEPT = 0x0101, /**< TM[1,1] successful acceptance verification report */
    TM0102_FAILED_ACCEPT = 0x0102, /**< TM[1,2] failed acceptance verification report */
    TM0103_SUCCESS_START = 0x0103, /**< TM[1,3] successful start of execution verification report */
    TM0104_FAILED_START = 0x0104, /**< TM[1,4] failed start of execution verification report */
    TM0105_SUCCESS_PROGRESS = 0x0105, /**< TM[1,5] successful progress of execution verification report */
    TM0106_FAILED_PROGRESS = 0x0106, /**< TM[1,6] failed progress of execution verification report */
    TM0107_SUCCESS_COMPLETE = 0x0107, /**< TM[1,7] successful completion of execution verification report */
    TM0108_FAILED_COMPLETE = 0x0108, /**< TM[1,8] failed completion of execution verification report */
} st01_subservice_t;

/** Failure details */
typedef struct st01_fail_details {
    fail_code_t code; /**< failure code */
    void* data; /**< deduced failure data */
    size_t data_size; /**< size of deduced failure data */
} st01_fail_details_t;

/** Structure of success report TM[1,1], TM[1,3], TM[1,7] */
typedef struct tm01xx_success_rep {
    uint8_t* tc_req_id; /**< first 4 bytes of secondary header of TC, to which we reply */
} tm01xx_success_rep_t;

/** Structure of failure report TM[1,2], TM[1,4], TM[1,8] */
typedef struct tm01xx_fail_rep {
    uint8_t* tc_req_id; /**< first 4 bytes of secondary header of TC, to which we reply */
    st01_fail_details_t fail; /**< failure details */
} tm01xx_fail_rep_t;

/** Structure of success report TM[1,5] */
typedef struct tm0105_success_rep {
    uint8_t* tc_req_id; /**< first 4 bytes of secondary header of TC, to which we reply */
    step_id_t step; /** step of progress */
} tm0105_success_rep_t;

/** Structure of failure report TM[1,6]*/
typedef struct tm0106_fail_rep {
    uint8_t* tc_req_id; /**< first 4 bytes of secondary header of TC, to which we reply */
    step_id_t step; /** step of progress */
    st01_fail_details_t fail; /**< failure details */
} tm0106_fail_rep_t;

/** Union of all supported ST[01] subservice data structures */
typedef union st01_struct {
    tm01xx_success_rep_t tm0101; /**< data structure for TM0101_SUCCESS_ACCEPT */
    tm01xx_fail_rep_t tm0102; /**< data structure for TM0102_FAILED_ACCEPT */
    tm01xx_success_rep_t tm0103; /**< data structure for TM0103_SUCCESS_START */
    tm01xx_fail_rep_t tm0104; /**< data structure for TM0104_FAILED_START */
    tm0105_success_rep_t tm0105; /**< data structure for M0105_SUCCESS_PROGRESS */
    tm0106_fail_rep_t tm0106; /**< data structure for TM0106_FAILED_PROGRESS */
    tm01xx_success_rep_t tm0107; /**< data structure for TM0107_SUCCESS_COMPLETE */
    tm01xx_fail_rep_t tm0108; /**< data structure for TM0108_FAILED_COMPLETE */
} st01_struct_t;

#endif /* INC_ST01_ACK_H_ */
