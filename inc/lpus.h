/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INC_LPUS_H_
#define INC_LPUS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "st01_ack.h"
#include "st03_house.h"
#include "st05_ev_rep.h"
#include "st08_func.h"

/** PUS packet version */
#define LPUS_PACKET_VERSION 2

/** Size of secodary header, TM */
#define LPUS_SEC_HDR_SIZE_TM    15
/** Size of secodary header, TM */
#define LPUS_SEC_HDR_SIZE_TC    5
/** Minimum size of PUS-specific secondary header */
#define LPUS_SEC_HDR_MIN_SIZE   LPUS_SEC_HDR_SIZE_TC
/** Maximum size of PUS-specific secondary header */
#define LPUS_SEC_HDR_MAX_SIZE   LPUS_SEC_HDR_SIZE_TM

/** Union of all supported service data structures */
typedef union lpus_struct {
    st01_struct_t st01; /**< data structures for ST[01] */
    st03_struct_t st03; /**< data structures for ST[03] */
    st05_struct_t st05; /**< data structures for ST[05] */
    st08_struct_t st08; /**< data structures for ST[08] */
} lpus_struct_t;

/**
 * Encode PUS message payload
 * Payload consists of PUS secondary header + user data
 * Payload should be provided to Space Packet as a `packet data field`
 *
 * @param[in] in_sub subservice enum
 * @param[in] in_struct input data structure
 * @param[out] out_buf output buffer to store encoded PUS payload
 * @param[in] buf_size size of output buffer
 *
 * @returns size of data in output buffer, or negative error code
 * @retval >= 0 size of data in output buffer
 * @retval -EINVAL invalid argument
 * @retval -EFBIG data too large to fit into output buffer
 */
int32_t lpus_encode(uint16_t in_sub, lpus_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size);

/**
 * Decode PUS message payload
 * Payload consists of PUS secondary header + user data
 * Payload should be provided from Space Packet as a `packet data field`

 * @param[in] in_buf input buffer that stores encoded PUS payload
 * @param[in] payload_size size of PUS payload in input buffer
 * @param[out] sub decoded subservice enum
 * @param[out] out_struct decoded data structure
 *
 * @returns size of data parsed from input buffer, or negative error code
 * @retval >= 0 size of parsed data from input buffer
 * @retval -EINVAL invalid argument
 */
int32_t lpus_decode(uint8_t* in_buf, size_t payload_size,
                    uint16_t* out_sub, lpus_struct_t* out_struct);

#ifdef __cplusplus
}
#endif

#endif /* INC_LPUS_H_ */
