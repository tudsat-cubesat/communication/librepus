/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TESTS_TEST_DATA_PRUST_H_
#define TESTS_TEST_DATA_PRUST_H_

#include <stdint.h>

/* From https://github.com/visionspacetec/Prust/wiki/How-to-Use-the-Client-Crate
 *
 * The packet send:
[24, 42, 192, 0, 0, 25, 32, 8, 1, 0, 0, 115, 101, 116, 95, 108, 101, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0]
TM SUCCESS RESPONSE
TM pack:
SpacePacket {
    primary_header: PrimaryHeader {
        ver_no: 0,
        type_flag: false,
        sec_header_flag: true,
        apid: 42,
        seq_flags: (
            true,
            true,
        ),
        packet_name: 0,
        data_len: 14,
    },
    data: TmPacket {
        header: TmPacketHeader {
            pus_ver_no: 2,
            time_ref_status: 0,
            service_type: 1,
            message_subtype: 7,
            message_type_counter: 0,
            destination_id: 42,
            abs_time: 0,
        },
        user_data: TxUserData {
            packet_error_control: 0,
            data: ServiceSuccess {
                request_id: RequestId {
                    ver_no: 0,
                    packet_type: true,
                    sec_header_flag: true,
                    apid: 42,
                    seq_flags: (
                        true,
                        true,
                    ),
                    packet_seq_count: 0,
                },
            },
        },
    },
}
The packet recieved (in bytes):
[8, 42, 192, 0, 0, 14, 32, 1, 7, 0, 0, 0, 42, 0, 0, 24, 42, 192, 0, 0, 0]
 */

static uint8_t prust_st08_packet[] = {
    24, 42, 192, 0, 0, 25, 32, 8,
    1, 0, 0, 115, 101, 116, 95, 108,
    101, 100, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 2, 0, 0, 0, 0
};

static uint8_t prust_st01_packet[] = {
    8, 42, 192, 0, 0, 18, /* primary header */
    32, 1, 7, 0, 0, 0, 42,
    0, 0, 0, 0, 0, 0, 0, 0, /* secondary header */
    24, 42, 192, 0 /* TM[1,7] data */
};

#endif /* TESTS_TEST_DATA_PRUST_H_ */
