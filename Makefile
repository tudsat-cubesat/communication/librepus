# Library for ECSS Package Utilization Standard (PUS)
#
# Copyright (C) 2020 TU Darmstadt Space Technology e.V.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

LPUS_LIB=liblibrepus.a

CC=$(CROSS_COMPILE)gcc
AR=$(CROSS_COMPILE)ar
CFLAGS=-Wall -Wextra -Werror

SRC_DIR=src
INC_DIR=inc
BUILD_DIR=build

SRC = $(wildcard $(SRC_DIR)/*.c)
OBJ = $(SRC:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)
OBJ_COV = $(SRC:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.ocov)
LIB_COV = $(BUILD_DIR)/$(LPUS_LIB)cov

all: mkdir_build $(LPUS_LIB) $(LIB_COV)

mkdir_build:
	mkdir -p $(BUILD_DIR)

$(LPUS_LIB): $(OBJ)
	$(AR) ru $@ $^

$(LIB_COV): $(OBJ_COV)
	$(AR) ru $@ $^

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -I$(INC_DIR) -c -o $@ $<

$(BUILD_DIR)/%.ocov: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -fprofile-arcs -ftest-coverage -fPIC -O0 \
		-I$(INC_DIR) -c -o $@ $<

.PHONY: clean

clean:
	-rm -rf $(BUILD_DIR) $(LPUS_LIB)
