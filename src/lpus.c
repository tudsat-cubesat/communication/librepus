/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>

#include "lpus.h"
#include "lpus_common.h"
#include "st01_ack_priv.h"
#include "st03_house_priv.h"
#include "st05_ev_rep_priv.h"
#include "st08_func_priv.h"

/** Mask of service number insde subservice number */
#define LPUS_SERVICE_MASK 0xFF00

/** PUS services */
typedef enum lpus_service {
    ST01_ACK = 0x0100, /**< ST[01] request verification reports (ACK / NACK) */
    ST03_HOUSE = 0x0300, /**< ST[03] housekeeping */
    ST05_EV_REP = 0x0500, /**< ST[05] event report */
    ST08_FUNC = 0x0800, /**< ST[08] perform functiont */
} lpus_service_t;

static int32_t lpus_encode_sec_hdr(uint16_t in_sub, bool is_tc,
                                   lpus_tc_ack_t* in_tc_ack,
                                   uint8_t out_buf[LPUS_SEC_HDR_MAX_SIZE])
{
    int32_t size = 0;

    out_buf[0] = LPUS_PACKET_VERSION << 4;
    out_buf[1] = (in_sub >> 8) & 0xFF; /* service */
    out_buf[2] = in_sub & 0xFF; /* subservice */

    if (is_tc) {
        if (!in_tc_ack)
            return (-EINVAL);

        /* TC sec hdr */
        out_buf[0] |= in_tc_ack->value & 0x0F;

        out_buf[3] = 0; /* TODO: support source ID */
        out_buf[4] = 0;

        size = LPUS_SEC_HDR_SIZE_TC;
    } else {
        /* TM sec hdr */
        /* TODO: support time reference status */
        out_buf[3] = 0; /* TODO: support counter */
        out_buf[4] = 0;
        out_buf[5] = 0; /* TODO: support destination ID */
        out_buf[6] = 0;
        /* TODO: support absolute time */
        out_buf[7] = 0;
        out_buf[8] = 0;
        out_buf[9] = 0;
        out_buf[10] = 0;
        out_buf[11] = 0;
        out_buf[12] = 0;
        out_buf[13] = 0;
        out_buf[14] = 0;

        size = LPUS_SEC_HDR_SIZE_TM;
    }

    return size;
}

static int32_t lpus_decode_sec_hdr_sub(uint8_t in_buf[LPUS_SEC_HDR_MAX_SIZE],
                                       size_t in_size, uint16_t* out_sub)
{
    /* verify version */
    if ((in_size < LPUS_SEC_HDR_MIN_SIZE) ||
        (((in_buf[0] >> 4) & 0x0F) != LPUS_PACKET_VERSION))
        return (-EINVAL);

    /* decode subservice */
    *out_sub = ((uint16_t)in_buf[1] << 8) | in_buf[2];

    return 0;
}

static int32_t lpus_decode_sec_hdr(uint8_t in_buf[LPUS_SEC_HDR_MAX_SIZE],
                                   size_t in_size, bool is_tc,
                                   lpus_tc_ack_t* out_tc_ack)
{
    /* verify version */
    if ((in_size < LPUS_SEC_HDR_MIN_SIZE) ||
        (((in_buf[0] >> 4) & 0x0F) != LPUS_PACKET_VERSION))
        return (-EINVAL);

    if (is_tc) {
        if (!out_tc_ack)
            return (-EINVAL);

        /* decode TC fields */
        out_tc_ack->value = in_buf[0] & 0x0F;
    }

    return (is_tc ? LPUS_SEC_HDR_SIZE_TC : LPUS_SEC_HDR_SIZE_TM);
}

int32_t lpus_encode(uint16_t in_sub, lpus_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size)
{
    int32_t sec_hdr_size = 0;
    int32_t user_data_size = 0;
    bool is_tc; /* true if TC, false if TM */
    lpus_tc_ack_t
    *tc_ack_p; /* poiter to ack flags in `in_struct`, used for TC packets */

    /* invalid arguments */
    if (!in_struct || !out_buf || (buf_size < LPUS_SEC_HDR_MAX_SIZE))
        return (-EINVAL);

    /* is TC? */
    switch (in_sub & LPUS_SERVICE_MASK) {
        case ST01_ACK:
            is_tc = st01_is_tc(in_sub, &in_struct->st01, &tc_ack_p);
            break;

        case ST03_HOUSE:
            is_tc = st03_is_tc(in_sub, &in_struct->st03, &tc_ack_p);
            break;

        case ST05_EV_REP:
            is_tc = st05_is_tc(in_sub, &in_struct->st05, &tc_ack_p);
            break;

        case ST08_FUNC:
            is_tc = st08_is_tc(in_sub, &in_struct->st08, &tc_ack_p);
            break;

        default:
            return (-EINVAL);
    }

    /* encode secondary header */
    sec_hdr_size = lpus_encode_sec_hdr(in_sub, is_tc, tc_ack_p, out_buf);
    if (sec_hdr_size < 0)
        return sec_hdr_size;

    /* encode application data */
    switch (in_sub & LPUS_SERVICE_MASK) {
        case ST01_ACK:
            user_data_size = st01_encode(in_sub, &in_struct->st01,
                                         (out_buf + sec_hdr_size),
                                         (buf_size - sec_hdr_size));
            break;

        case ST03_HOUSE:
            user_data_size = st03_encode(in_sub, &in_struct->st03,
                                         (out_buf + sec_hdr_size),
                                         (buf_size - sec_hdr_size));
            break;

        case ST05_EV_REP:
            user_data_size = st05_encode(in_sub, &in_struct->st05,
                                         (out_buf + sec_hdr_size),
                                         (buf_size - sec_hdr_size));
            break;

        case ST08_FUNC:
            user_data_size = st08_encode(in_sub, &in_struct->st08,
                                         (out_buf + sec_hdr_size),
                                         (buf_size - sec_hdr_size));
            break;

        default:
            return (-EINVAL);
    }

    if (user_data_size < 0)
        return user_data_size;

    return (sec_hdr_size + user_data_size);
}

int32_t lpus_decode(uint8_t* in_buf, size_t payload_size,
                    uint16_t* out_sub, lpus_struct_t* out_struct)
{
    int32_t sec_hdr_size = 0;
    int32_t user_data_size = 0;
    bool is_tc; /* true if TC, false if TM */
    lpus_tc_ack_t
    *tc_ack_p; /* poiter to ack flags in `out_struct`, used for TC packets */

    /* invalid arguments */
    if (!out_struct || !out_sub || !in_buf ||
        (payload_size < LPUS_SEC_HDR_MIN_SIZE))
        return (-EINVAL);

    /* decode secondary header subservice */
    sec_hdr_size = lpus_decode_sec_hdr_sub(in_buf, payload_size, out_sub);
    if (sec_hdr_size < 0)
        return sec_hdr_size;

    /* is TC? */
    switch ((*out_sub) & LPUS_SERVICE_MASK) {
        case ST01_ACK:
            is_tc = st01_is_tc((*out_sub), &out_struct->st01, &tc_ack_p);
            break;

        case ST03_HOUSE:
            is_tc = st03_is_tc((*out_sub), &out_struct->st03, &tc_ack_p);
            break;

        case ST05_EV_REP:
            is_tc = st05_is_tc((*out_sub), &out_struct->st05, &tc_ack_p);
            break;

        case ST08_FUNC:
            is_tc = st08_is_tc((*out_sub), &out_struct->st08, &tc_ack_p);
            break;

        default:
            return (-EINVAL);
    }

    /* decode secondary header */
    sec_hdr_size = lpus_decode_sec_hdr(in_buf, payload_size, is_tc, tc_ack_p);
    if (sec_hdr_size < 0)
        return sec_hdr_size;

    /* decode application data */
    switch ((*out_sub) & LPUS_SERVICE_MASK) {
        case ST01_ACK:
            user_data_size = st01_decode((*out_sub), (in_buf + sec_hdr_size),
                                         (payload_size - sec_hdr_size),
                                         &out_struct->st01);
            break;

        case ST03_HOUSE:
            user_data_size = st03_decode((*out_sub), (in_buf + sec_hdr_size),
                                         (payload_size - sec_hdr_size),
                                         &out_struct->st03);
            break;

        case ST05_EV_REP:
            user_data_size = st05_decode((*out_sub), (in_buf + sec_hdr_size),
                                         (payload_size - sec_hdr_size),
                                         &out_struct->st05);
            break;

        case ST08_FUNC:
            user_data_size = st08_decode((*out_sub), (in_buf + sec_hdr_size),
                                         (payload_size - sec_hdr_size),
                                         &out_struct->st08);
            break;

        default:
            return (-EINVAL);
    }

    if (user_data_size < 0)
        return user_data_size;

    return (sec_hdr_size + user_data_size);
}
