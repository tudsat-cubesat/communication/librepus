/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SRC_ST08_FUNC_PRIV_H_
#define SRC_ST08_FUNC_PRIV_H_

#include <stdbool.h>

#include "lpus_common.h"
#include "st08_func.h"

/* Is subservice TC or not (TM) */
bool st08_is_tc(st08_subservice_t in_sub, st08_struct_t* in_struct,
                lpus_tc_ack_t** out_tc_ack);

/* Encode ST[08] function */
int32_t st08_encode(st08_subservice_t in_sub, st08_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size);

/* Decode ST[08] function */
int32_t st08_decode(st08_subservice_t in_sub, uint8_t* in_buf,
                    size_t data_size, st08_struct_t* out_struct);

#endif /* SRC_ST08_FUNC_PRIV_H_ */
