/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "st08_func_priv.h"

bool st08_is_tc(st08_subservice_t in_sub, st08_struct_t* in_struct,
                lpus_tc_ack_t** out_tc_ack)
{
    (void)in_sub;
    (void)in_struct;

    if (in_struct && out_tc_ack)
        *out_tc_ack = &in_struct->tc0801.tc_ack;

    /* the only subservice in ST[08] is TC */
    return true;
}

int32_t st08_encode(st08_subservice_t in_sub, st08_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size)
{
    (void)in_sub;

    int32_t size = 0;
    tc0801_func_t* tc0801;

    if (!in_struct || !out_buf || !in_struct->tc0801.args)
        return (-EINVAL);

    tc0801 = &in_struct->tc0801;

    /* function ID */
    if (sizeof(tc0801->func_id) > buf_size)
        return (-EFBIG);

    memset(out_buf, 0, sizeof(tc0801->func_id));
    strncpy((char*)out_buf, tc0801->func_id, ST08_FUNC_ID_SIZE);

    size += sizeof(tc0801->func_id);

    /* number of arguments */
    if ((size + sizeof(tc0801->args_num)) > buf_size)
        return (-EFBIG);

    memcpy((out_buf + size), &tc0801->args_num, sizeof(tc0801->args_num));

    size += sizeof(tc0801->args_num);

    /* arguments */
    if ((size + tc0801->args_size) > buf_size)
        return (-EFBIG);

    memcpy((out_buf + size), tc0801->args, tc0801->args_size);

    size += sizeof(tc0801->args_size);

    return size;
}

int32_t st08_decode(st08_subservice_t in_sub, uint8_t* in_buf,
                    size_t data_size, st08_struct_t* out_struct)
{
    (void)in_sub;

    int32_t size = 0;
    tc0801_func_t* tc0801;

    if (!in_buf || !out_struct)
        return (-EINVAL);

    tc0801 = &out_struct->tc0801;

    /* function ID */
    if (sizeof(tc0801->func_id) > data_size)
        return (-EINVAL);

    memcpy(tc0801->func_id, (char*)in_buf, sizeof(tc0801->func_id));

    size += sizeof(tc0801->func_id);

    /* number of arguments */
    if ((size + sizeof(tc0801->args_num)) > data_size)
        return (-EINVAL);

    memcpy(&tc0801->args_num, (in_buf + size), sizeof(tc0801->args_num));

    size += sizeof(tc0801->args_num);

    /* arguments */
    tc0801->args_size = data_size - size;
    tc0801->args = in_buf + size;

    size = data_size;

    return size;
}
