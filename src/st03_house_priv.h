/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2021 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Service type ST[03] housekeeping
 * Private header
 */

#ifndef SRC_ST03_HOUSE_PRIV_H_
#define SRC_ST03_HOUSE_PRIV_H_

#include <stdbool.h>

#include "lpus_common.h"
#include "st03_house.h"

/* Is suservice TC or not (TM) */
bool st03_is_tc(st03_subservice_t in_sub, st03_struct_t* in_struct,
                lpus_tc_ack_t** out_tc_ack);

/* Encode ST[03] housekeeping */
int32_t st03_encode(st03_subservice_t in_sub, st03_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size);

/* Decode ST[03] housekeeping */
int32_t st03_decode(st03_subservice_t in_sub, uint8_t* in_buf,
                    size_t data_size, st03_struct_t* out_struct);

#endif /* SRC_ST03_HOUSE_PRIV_H_ */
